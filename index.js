// lession 1
function tnChiuThue(a,b){
    var tong = a - 4e+6 - b*1.6e+6;
    return tong;
}

function tinhThue(){
    var ten = document.getElementById("txt-ten").value;
    var tn = document.getElementById("txt-thuNhap").value*1;
    var soNguoi = document.getElementById("txt-nguoi").value*1;
    var result1 = document.getElementById("result1");
    var tongThue = tnChiuThue(tn,soNguoi);
    
    if(0<tongThue && tongThue<=60e+6){
        tongThue *=0.05 ;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if(60e+6<tongThue && tongThue<=120e+6){
        tongThue *=0.1 ;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if(120e+6<tongThue && tongThue<=210e+6){
        tongThue *=0.15 ;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if (210e+6<tongThue && tongThue<=384e+6){
        tongThue *=0.2;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if(384e+6<tongThue && tongThue<=624e+6){
        tongThue *=0.25;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if(624e+6<tongThue && tongThue<=960e+6){
        tongThue *=0.3;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else if(960e+6<tongThue){
        tongThue *= 0.35;
        result1.innerHTML=`<h3>
        Họ và Tên: ${ten}; Tiền thuế thu nhập cá nhân: ${tongThue.toLocaleString()} VNĐ.
        </h3>`
    }else{
        alert ("Dữ liệu Tổng Thu Nhập Không Hợp Lệ");
    }
    
    if (soNguoi<0){
        alert ("Dữ liệu Người Phụ Thuộc Không Hợp Lệ");
    }
}

// lession 2

function xuLyDisplay(){
    var xuLy = document.getElementById("txt-soKn");
    var loaiKh = document.getElementById("txt-Kh").value*1;
    if (loaiKh == 2){
        xuLy.style.display = "block";
    }else{
        xuLy.style.display = "none"; 
    }
}

function tienTren10Kn(a,b){
    var tien = 15 + 75 + (a-10)*5 + b*50;
    return tien;
}

function tinhTien(){
    var maKh = document.getElementById("txt-maKh").value;
    var loaiKh = document.getElementById("txt-Kh").value*1;
    var soKenhcc = document.getElementById("txt-kenhCaoCap").value*1;
    var soKn = document.getElementById("txt-soKn").value*1;
    var result2 = document.getElementById("result2");
    var tong = 0;
    switch (loaiKh){
        case 0:{
            alert("Hãy Chọn loại Khách Hàng")
        }break;
        case 1:{
            if(soKenhcc>=0){
             tong = 4.5 + 20.5 + soKenhcc*7.5;
             
             result2.innerHTML=`<h3> Mã Khách Hàng: ${maKh};
             Tiền cáp: ${Intl.NumberFormat('de-DE', { style: 'currency', currency: 'USD' }).format(tong)}
             </h3>`

            }else{
                alert("Dữ Liệu Số Kênh Cao Cấp Không Hợp Lý");
            }
        }break;
        case 2:{
           if (0<= soKn && soKn<=10){
             tong = 15 + 75 + 50*soKenhcc;

             result2.innerHTML=`<h3> Mã Khách Hàng: ${maKh};
             Tiền cáp: ${Intl.NumberFormat('de-DE', { style: 'currency', currency: 'USD' }).format(tong)}
             </h3>`

           }else if(10<soKn){
            tong = tienTren10Kn(soKn,soKenhcc);
            
            result2.innerHTML=`<h3> Mã Khách Hàng: ${maKh};
             Tiền cáp: ${Intl.NumberFormat('de-DE', { style: 'currency', currency: 'USD' }).format(tong)}
             </h3>`

           }else{
            alert("Dữ Liệu Số Kết nối Không Hợp Lý");
           }
           if(soKenhcc<0){
            alert("Dữ Liệu Số Kênh Cao Cấp Không Hợp Lý");
           }
        }break;
    }
}
